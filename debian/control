Source: meme
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Steffen Moeller <moeller@debian.org>
Section: non-free/science
XS-Autobuild: no
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               libxslt1-dev,
               python3-all,
               zlib1g-dev,
               elinks,
               help2man,
               ghostscript
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/med-team/meme
Vcs-Git: https://salsa.debian.org/med-team/meme.git
Homepage: https://meme-suite.org/
Rules-Requires-Root: no

Package: meme
Architecture: any
Depends: ${python:Depends},
         ${shlibs:Depends},
         ${misc:Depends},
         csh,
         readseq,
         ghostscript,
         imagemagick | graphicsmagick
Description: search for common motifs in DNA or protein sequences
 MEME (Multiple EM for Motif Elicitation) is a tool for discovering
 motifs in a group of related DNA or protein sequences.  A motif is a
 sequence pattern that occurs repeatedly in a group of related protein or
 DNA sequences. MEME represents motifs as position-dependent
 letter-probability matrices which describe the probability of each
 possible letter at each position in the pattern. Individual MEME motifs
 do not contain gaps. Patterns with variable-length gaps are split by
 MEME into two or more separate motifs.
 .
 MEME takes as input a group of DNA or protein sequences (the training set)
 and outputs as many motifs as requested. MEME uses statistical modeling
 techniques to automatically choose the best width, number of occurrences,
 and description for each motif.

Package: glam2
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${python:Depends}
Description: gapped protein motifs from unaligned sequences
 GLAM2 is a software package for finding motifs in sequences, typically
 amino-acid or nucleotide sequences. A motif is a re-occurring sequence
 pattern: typical examples are the TATA box and the CAAX prenylation motif. The
 main innovation of GLAM2 is that it allows insertions and deletions in motifs.
 .
 This package includes programs for discovering motifs shared by a set of
 sequences and finding matches to these motifs in a sequence database, as well
 as utilities for converting glam2 motifs to standard alignment formats,
 masking glam2 motifs out of sequences so that weaker motifs can be found, and
 removing highly similar members of a set of sequences.
 .
 In this binary package, the fast Fourier algorithm (FFT) was enabled for the
 glam2 program.
